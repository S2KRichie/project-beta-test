import React, { useState } from 'react';

function AddCustomerForm() {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [address, setAddress] = useState('');
    const [phone_number, setPhone] = useState('');

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
        first_name: firstName,
        last_name: lastName,
        address,
        phone_number,
        };

        const url = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
        };

        const response = await fetch(url, fetchConfig);
        if(response.ok) {
        setFirstName('');
        setLastName('');
        setAddress('');
        setPhone('');
        }
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a new customer</h1>
            <form onSubmit={handleSubmit} id="create-customer-form">

                <div className="form-floating mb-3">
                <input onChange={e => setFirstName(e.target.value)} value={firstName} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control" />
                <label htmlFor="first_name">First Name</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={e => setLastName(e.target.value)} value={lastName} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" />
                <label htmlFor="last_name">Last Name</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={e => setAddress(e.target.value)} value={address} placeholder="Customer Address" required type="text" name="address" id="address" className="form-control" />
                <label htmlFor="address">Customer Address</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={e => setPhone(e.target.value)} value={phone_number} placeholder="Customer Phone" required type="text" name="phone" id="phone" className="form-control" />
                <label htmlFor="phone">Customer Phone</label>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
        </div>
    );
    }

    export default AddCustomerForm;
