import React, { useEffect, useState } from "react";

function ServiceHistory() {
  const [appointments, setAppointments] = useState([]);
  const [searchVin, setSearchVin] = useState('');
  const [automobiles, setAutomobiles] = useState([]);

  const checkVIP = (vin) => {
    return automobiles && automobiles.length > 0 && automobiles.some(auto => auto.vin === vin);
  }

  const handleSearchVinChange = (e) => {
    setSearchVin(e.target.value)
  }

  const handleSearchVin = async (event) => {
    event.preventDefault();
    const url = 'http://localhost:8080/api/appointments/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      for (let appointment of data.appointments) {
        const date = new Date(appointment.date_time).toLocaleDateString();
        appointment["date"] = date;
      }
      setAppointments(updatedAppointments => updatedAppointments.filter(appointment => appointment.vin === searchVin));
    }

    const autoUrl = 'http://localhost:8080/api/automobileVOs/';
    const autoResponse = await fetch(autoUrl);
    if (autoResponse.ok) {
      const autoData = await autoResponse.json();
      setAutomobiles(autoData.AutomobilesVOs);
    }
  }

  const fetchData = async () => {
    const url = 'http://localhost:8080/api/appointments/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
      for (let appointment of data.appointments) {
        const date = new Date(appointment.date_time).toLocaleDateString();
        appointment["date"] = date;
        const time = new Date(appointment.date_time).toLocaleTimeString();
        appointment["time"] = time;
      }
    }

    const autoUrl = 'http://localhost:8080/api/automobileVOs/';
    const autoResponse = await fetch(autoUrl);
    if (autoResponse.ok) {
      const autoData = await autoResponse.json();
      setAutomobiles(autoData.AutomobileVOs);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      <h1>Service History</h1>
      <form className='col' onSubmit={handleSearchVin}>
        <input className="form-control" id="searchbar" placeholder="Search by VIN..." value={searchVin} onChange={handleSearchVinChange}></input>
        <button className="btn btn-primary btn-sm">Search</button>
      </form>
      <table className='table table-striped'>
        <thead>
          <tr>
            <th>VIN</th>
            <th>is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Technician</th>
            <th>Reason</th>
          </tr>
        </thead>
        <tbody>
          {appointments.map((appointment) => (
            <tr key={appointment.id}>
              <td>{appointment.vin}</td>
              <td>{checkVIP(appointment.vin) ? 'Yes' : 'No'}</td>
              <td>{appointment.customer}</td>
              <td>{appointment.date_time}</td>
              <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
              <td>{appointment.reason}</td>
              <td>{appointment.status}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}

export default ServiceHistory;
