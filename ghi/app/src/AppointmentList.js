import React, { useEffect, useState } from "react";

function AppointmentList() {
    const [appointments, setAppointments] = useState([])
    const [automobiles, setAutomobiles] = useState([])

    const fetchData = async () => {
        const appointmentUrl = 'http://localhost:8080/api/appointments/'
        const appointmentResponse = await fetch(appointmentUrl);

        if (appointmentResponse.ok) {
            const appointmentData = await appointmentResponse.json();
            const formattedAppointments = appointmentData.appointments.map(appointment => ({
                ...appointment,
                date: new Date(appointment.date_time).toLocaleDateString()
            }));
            setAppointments(formattedAppointments);
        }

        const autoUrl = "http://localhost:8080/api/automobileVOs/"
        const autoResponse = await fetch(autoUrl)

        if (autoResponse.ok) {
            const automobileData = await autoResponse.json();
            setAutomobiles(automobileData.AutomobileVOs);
        }
    }


    const checkVIP = (vin) => {
        return automobiles.some(auto => auto.vin === vin);
    }

    const handleCancel = async (id) => {
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/canceled`, { 'method': 'PUT' });
        if (response.ok) {
            setAppointments(updatedAppointments => updatedAppointments.filter(appointment => appointment.id !== id));
        }
    }

    const handleFinish = async (id) => {
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/finished`, { 'method': 'PUT' });
        if (response.ok) {
            setAppointments(updatedAppointments => updatedAppointments.filter(appointment => appointment.id !== id));
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>is VIP?</th>
                        <th>Customer</th>
                        <th>Date & Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map((appointment) => {
                        return (
                            <tr key={appointment.id}>
                                <td>{appointment.vin}</td>
                                <td>{checkVIP(appointment.vin) ? 'Yes' : 'No'}</td>
                                <td>{appointment.customer}</td>
                                <td>{appointment.date_time}</td>
                                <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                                <td>{appointment.reason}</td>
                                <td><button type="button" className="btn btn-danger" onClick={() => handleCancel(appointment.id)}>Cancel</button></td>
                                <td><button type="button" className="btn btn-success" onClick={() => handleFinish(appointment.id)}>Finish</button></td>

                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    );
}
export default AppointmentList;
