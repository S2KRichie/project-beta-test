import React, { useState, useEffect } from 'react';

function InventoryForm() {
    const [model_id, setModel] = useState('');
    const [color, setColor] = useState('');
    const [vin, setVin] = useState('');
    const [year, setYear] = useState('');
    const [models, setModels] = useState([]);

    useEffect(() => {
      async function getModels() {
        const url = 'http://localhost:8100/api/models/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setModels(data.models);
        }
      }
      getModels();
    }, []);

    async function handleSubmit(event) {
      event.preventDefault();
      const data = {
        model_id,
        color,
        vin,
        year,
      };

      const locationUrl = 'http://localhost:8100/api/automobiles/';
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const response = await fetch(locationUrl, fetchConfig);
      if(response.ok) {
        setModel('');
        setColor('');
        setVin('');
        setYear('');
      }
    }

    const handleChangeModel = (event) => {
      const value = event.target.value;
      setModel(value);
    }

    const handleChangeColor = (event) => {
      const value = event.target.value;
      setColor(value);
    }

    const handleChangeVin = (event) => {
      const value = event.target.value;
      setVin(value);
    }

    const handleChangeYear = (event) => {
      const value = event.target.value;
      setYear(value);
    }

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new automobile</h1>
            <form onSubmit={handleSubmit} id="create-automobile-form">

              <div className="form-floating mb-3">
                <input onChange={handleChangeColor} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleChangeYear} value={year} placeholder="Year" required type="text" name="year" id="year" className="form-control" />
                <label htmlFor="year">Year</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleChangeVin} value={vin} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" />
                <label htmlFor="vin">VIN</label>
              </div>
              <div className="mb-3">
                <select onChange={handleChangeModel} value={model_id} required name="model" id="model" className="form-select">
                  <option value="">Choose a model</option>
                  {models.map((model) => {
                    return (
                      <option key={model.id} value={model.id}>{model.name}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
  }

  export default InventoryForm;