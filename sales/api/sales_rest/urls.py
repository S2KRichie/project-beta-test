from django.urls import path
from .views import SalespersonSales, api_view_automobile, api_list_sales, api_view_sale, api_list_salespeople, api_view_salespeople, api_view_customer, api_list_customers
urlpatterns = [
    # Salespeople
    path('salespeople/', api_list_salespeople, name='api_list_salespeople'),
    path('salespeople/<int:id>', api_view_salespeople, name='api_view_salespeople'),

    # Customers
    path('customers/', api_list_customers, name='api_list_customers'),
    path('customers/<int:id>', api_view_customer, name='api_view_customer'),

    # Sales
    path('sales/', api_list_sales, name='api_list_sales'),
    path('sales/<int:id>', api_view_sale, name='api_view_sale'),
    path('salesperson/<int:id>/sales/', SalespersonSales.as_view(), name='salesperson_sales'),

    # Auto
    path('automobiles/<str:vin>/', api_view_automobile, name='automobile-detail'),
]